<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Directory;
use AppBundle\Form\DirectoryType;

/**
 * Description of DirectoryController
 *
 * @author Gaël
 */
class DirectoryController extends Controller {

    /**
     * @Route("/directory/new", name="app_directory_new")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request) {

        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        $userId = $this->getUser()->getId();
        $directory = new Directory();
        $form = $this->createForm(DirectoryType::class, $directory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $directory->setUser($userId);
            $directory->setSize(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($directory);
            $em->flush();
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('paper-dashboard/new.html.twig', array(
                    'form' => $form->createView(),
                    'title_page' => 'Créer un dossier'
        ));
    }

    /**
     * @Route("/directories", name="app_directory_listing")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request) {

        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        $userId = $this->getUser()->getId();
        $directoryRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Directory');
        $directories = $directoryRepository->findBy(array('user' => $userId));
        $userManager = $this->container->get('fos_user.user_manager');
        foreach ($directories as $element) {
            $element->setUser($userManager->findUserBy(array('id' => $element->getUser()))->getEmail());
        }

        return $this->render('paper-dashboard/directory-table.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                    'title_page' => 'Dossiers',
                    'directories' => $directories,
                    'nb_directories' => count($directories)
        ]);
    }

}
