<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {

        if (is_null($this->getUser())) {
            return $this->render('supfile/index.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR
            ]);
        } else {
            
            $userId = $this->getUser()->getId();
            $fileRepository = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:FileUpload');
            
            $files = $fileRepository->findBy(array('userId' => $userId));
            $recentFiles = $fileRepository->findBy(array('userId' => $userId), array('id' =>'DESC'), 8);
            $size = 0;
            
            foreach ($files as $file){
                $size += $file->getSize();
            }
            
            $directoryRepository = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Directory');
            $directories = $directoryRepository->findBy(array('user' => $userId));

            return $this->render('paper-dashboard/index.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                'title_page' => 'Dashboard',
                'space_used' => $size,
                'nb_files' => count($files),
                'nb_directories' => count($directories),
                'recent_files' => $recentFiles
            ]);
        }
    }
    /**
     * @Route("/user-profile", name="user_profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction(Request $request) {

        if (is_null($this->getUser())) {
            return $this->render('supfile/index.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR
            ]);
        } else {
            return $this->render('paper-dashboard/user.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                'title_page' => 'Profil'
            ]);
        }
    }
    
}