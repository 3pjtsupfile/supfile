<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\FileUpload;
use AppBundle\Form\FileUploadType;
use phpseclib\Net\SSH2;
use phpseclib\Net\SFTP;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Description of FileController
 *
 * @author Gaël
 */
class FileController extends Controller {

    /**
     * @Route("/files", name="file_listing")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tableAction() {
        
        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }
        
        $user = $this->getUser();
        $userId = $user->getId();
        $fileRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:FileUpload');

        $files = $fileRepository->findBy(array('userId' => $userId));

        $userManager = $this->container->get('fos_user.user_manager');
        
        $directoryRepository = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Directory');
        $directories = $directoryRepository->findBy(array('user' => $userId));

        foreach ($files as $file) {
            $file->user = $userManager->findUserBy(array('id' => $file->getUserId()))->getEmail();
            if(is_null($file->getDirectoryId())){
                $file->directory = '';
            }
            else{
                $file->directory = $directoryRepository->findOneBy(array('id' => $file->getDirectoryId()))->getName();
            }
        }

        return $this->render('paper-dashboard/table.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                    'files' => $files,
                    'title_page' => 'Fichiers',
        ]);
    }
    
    /**
     * @Route("{idDirectory}/files", name="file_directory_listing")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tableDirectoryAction($idDirectory) {
        
        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }
        
        $user = $this->getUser();
        $userId = $user->getId();
        $fileRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:FileUpload');
        
        $directoryRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Directory');
        
        $directory = $directoryRepository->find((int) $idDirectory);

        $files = $fileRepository->findBy(array("directoryId" =>(int) $idDirectory));
        $userManager = $this->container->get('fos_user.user_manager');
        foreach ($files as $file) {
            $file->user = $userManager->findUserBy(array('id' => $file->getUserId()))->getEmail();
        }

        return $this->render('paper-dashboard/file-directory-table.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
                    'files' => $files,
                    'title_page' => 'Fichiers',
                    'directory' => $directory
        ]);
    }

    /**
     * @Route("/file/new", name="app_file_new")
     */
    public function newAction(Request $request) {

        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }
        $userId = $this->getUser()->getId();
        $fileToUpload = new FileUpload();
        $form = $this->createForm(FileUploadType::class, $fileToUpload);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $file = $fileToUpload->getFile();
            $fileName = str_replace(' ', '_', $file->getClientOriginalName());        
            $fileSize = round($file->getClientSize() / 1024 / 1024, 2); // calcul la taille en Mo
            $fileSizeAsString = round((string) $fileSize, 2);
            $fileToUpload->setSizeAsString($fileSizeAsString . ' Mo');
            $fileToUpload->setSize($fileSize);
            $fileToUpload->setName(substr($fileName, 0, strrpos($fileName, "."))); // garde uniquement le nom du fichier
            $fileToUpload->setFile($fileName);
            $fileToUpload->setExtension(str_replace(substr($fileName, 0, strrpos($fileName, ".")), '', $fileName));
            $fileToUpload->setUploadedAt();
            
            $localFilePath = $this->getParameter('kernel.project_dir') . "/web/uploads/";
            $fileToUpload->setUserId($userId);

            $file->move(
                    $localFilePath, $fileName
            );
            $sftp = new SFTP($this->getParameter('ceph.ip'));
        
            if (!$sftp->login('cephuser', 'toor')) {
                throw new \Exception('Cannot login into your server !');
            }
            
            $remoteFile = '/home/cephuser/upload/'.$fileName;
                       
            $exec = $sftp->put($remoteFile, $localFilePath.$fileName, SFTP::SOURCE_LOCAL_FILE);
            $execftp = $this->putFile('user_'.$userId, $fileName); // ajout dans une pool ceph
            if($exec == true && $execftp == true){
                $fileToUpload->setFile($fileName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($fileToUpload);
                $em->flush();
            }
            else
            {
                throw new \Exception('Upload Failed');
            }            
            return $this->redirect($this->generateUrl('file_listing'));
        }
        return $this->render('paper-dashboard/new.html.twig', array(
                    'form' => $form->createView(),
                    'title_page' => 'Upload de fichiers'
        ));
    }
    
    /**
     * @Route("/file/new/{userId}/{directoryId}", name="app_file_directory")
     */
    public function newFileDirectory($userId, $directoryId, Request $request ) {
        
        if (is_null($this->getUser())) {
            return $this->redirect($this->generateUrl('homepage'));
        }
        $fileToUpload = new FileUpload();
        $form = $this->createForm(FileUploadType::class, $fileToUpload);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $file = $fileToUpload->getFile();
            $fileName = str_replace(' ', '_', $file->getClientOriginalName());
            $fileSize = round($file->getClientSize() / 1024 / 1024, 2); // calcul la taille en Mo
            $fileSizeAsString = round((string) $fileSize, 2);
            $fileToUpload->setSizeAsString($fileSizeAsString . ' Mo');
            $fileToUpload->setSize($fileSize);
            $fileToUpload->setName(substr($fileName, 0, strrpos($fileName, "."))); // garde uniquement le nom du fichier
            $fileToUpload->setFile($fileName);
            $fileToUpload->setExtension(str_replace(substr($fileName, 0, strrpos($fileName, ".")), '', $fileName));
            $fileToUpload->setUploadedAt();
            $fileToUpload->setDirectoryId($directoryId);

            $localFilePath = $this->getParameter('kernel.project_dir') . "/web/uploads/";
            $fileToUpload->setUserId($userId);

            $file->move(
                    $localFilePath, $fileName
            );
            $sftp = new SFTP($this->getParameter('ceph.ip'));

            if (!$sftp->login('cephuser', 'toor')) {
                throw new \Exception('Cannot login into your server !');
            }

            $remoteFile = '/home/cephuser/upload/' . $fileName;

            $exec = $sftp->put($remoteFile, $localFilePath . $fileName, SFTP::SOURCE_LOCAL_FILE);
            $execftp = $this->putFile('user_' . $userId, $fileName); // ajout dans une pool ceph
            if ($exec == true && $execftp == true) {
                $fileToUpload->setFile($fileName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($fileToUpload);
                $em->flush();
            } else {
                throw new \Exception('Upload Failed');
            }
            return $this->redirect($this->generateUrl('file_directory_listing', array('idDirectory' => $directoryId)));
        }
        return $this->render('paper-dashboard/new-file-dir.html.twig', array(
                    'form' => $form->createView(),
                    'title_page' => 'Upload de fichiers'
        ));
    }

    /**
     * @Route("/file/ssh", name="app_file_ssh")
     */
    public function testSshConnect($userId = 'test') {

        $sftp = new SSH2($this->getParameter('ceph.ip'));

        if (!$sftp->login('cephuser', 'toor')) {
            throw new \Exception('Cannot login into your server !');
        }
        dump($sftp->exec('ls /home/cephuser/'));
        dump($sftp);
        die;
        return $this->render('supfile/index.html', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR
        ]);
    }

    /**
     * @Route("/file/addObjaddFile", name="app_file_addobj")
     */
    public function putFile($user, $file) {
        
        if ($this->getUser()->getId() != $user) {
            return $this->redirectToRoute('homepage');
        }

        $file = str_replace(' ', '_', $file);
        $ssh = new SSH2($this->getParameter('ceph.ip'));
        
        if (!$ssh->login('cephuser', 'toor')) {
            throw new \Exception('Cannot login into your server !');
        }
        $exec = $ssh->exec("cd /home/cephuser/script && sudo ./putObjet.sh $user '$file'");
        $localFile = $this->getParameter('kernel.project_dir')."/web/uploads/$file";
        $ssh->exec("sudo rm /home/cephuser/upload/$file");
        

        $rmLocal = new Process("rm $localFile");
        $rmLocal->run();
        if (!$rmLocal->isSuccessful()) {
            throw new ProcessFailedException($rmLocal);
        }

        return $this->render('supfile/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR
        ]);
    }

    /**
     * @Route("/file/getFile/{userId}/{fileName}", name="app_file_get")
     */
    public function getFile($userId, $fileName) {
        if ($this->getUser()->getId() != $userId) {
            return $this->redirectToRoute('homepage');
        }
        $fileName = str_replace(' ', '_', $fileName);
        $sftp = new SFTP($this->getParameter('ceph.ip'));

        if (!$sftp->login('cephuser', 'toor')) {
            throw new \Exception('Erreur de connexion');
        }
        $remoteFilePath = "/home/cephuser/download/$fileName";
        $localFilePath = $this->getParameter('kernel.project_dir') . "/web/downloads/$fileName";
        $exec = $sftp->exec("cd /home/cephuser/script && sudo ./getObjet.sh user_$userId '$fileName'");
        
        $get = $sftp->get($remoteFilePath, $localFilePath);
        $sftp->exec("sudo rm $remoteFilePath");
        

        /*$rmLocal = new Process("rm $localFilePath");
        $rmLocal->run();
        if (!$rmLocal->isSuccessful()) {
            throw new ProcessFailedException($rmLocal);
        }*/

        return $this->file($localFilePath);
    }

    /**
     * @Route("/file/rename/{userId}/{fileId}", name="app_file_rename")
     */
    public function renameAction($userId, $fileId, Request $request) {
        
        if ($this->getUser()->getId() != $userId) {
            return $this->redirectToRoute('homepage');
        }
        
        $fileRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:FileUpload');
        $file = $fileRepository->findOneBy(array('id' => $fileId));
        
        $form = $this->createFormBuilder($file)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Renommer le fichier'))
            ->getForm();
        
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $name = $form->get('name')->getData();
            $name = str_replace(' ', '_', $name);
            $renameFile = "$name".$file->getExtension();
            $oldName = $file->getFile();
            $sftp = new SFTP($this->getParameter('ceph.ip'));

            if (!$sftp->login('cephuser', 'toor')) {
                throw new \Exception('Erreur de connexion');
            }
            
            $userId = "user_$userId";
            $exec = $sftp->exec("cd /home/cephuser/script && sudo ./renameObjet.sh $userId '$oldName' $renameFile");
            $file->setName($name);
            $file->setFile($renameFile);
                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($file);
            $em->flush();
            return $this->redirect($this->generateUrl('file_listing'));
        }
        return $this->render('paper-dashboard/rename.html.twig', array(
                    'form' => $form->createView(),
                    'title_page' => 'Renommer un fichier'
        ));
    }
    
    /**
     * @Route("/file/delete/{userId}/{fileId}", name="app_file_delete")
     */
    public function deleteAction($userId, $fileId) {
        
        if ($this->getUser()->getId() != $userId) {
            return $this->redirectToRoute('homepage');
        }
        
        $fileRepository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:FileUpload');
        $file = $fileRepository->findOneBy(array('id' => $fileId));

        $fileName = $file->getFile();
        $sftp = new SFTP($this->getParameter('ceph.ip'));

        if (!$sftp->login('cephuser', 'toor')) {
            throw new \Exception('Erreur de connexion');
        }

        $userId = "user_$userId";
        $exec = $sftp->exec("cd /home/cephuser/script && sudo ./removeObjet.sh $userId $fileName");

        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();
        return $this->redirect($this->generateUrl('file_listing'));
    }

}
