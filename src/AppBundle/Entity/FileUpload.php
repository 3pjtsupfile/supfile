<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileUploadRepository")
 */
class FileUpload
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload a file.")
     */
    private $file;
    
    /**
     * @ORM\Column(type="string")
     * 
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Extension", type="string", length=255)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="Size_string", type="string", length=255)
     */
    private $sizeAsString;
    
    /**
     * @var float
     *
     * @ORM\Column(name="Size", type="decimal", precision=65, scale=2)
     */
    private $size;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    //private $user; A Remettre plus tard
    
    /**
     * @var integer
     * @ORM\Column(name="UserID", type="integer")
     */
    private $userId;
    
    /**
     * @Assert\DateTime()
     * @ORM\Column(name="upload_date", type="datetime")
     */
    protected $uploadedAt;

    /**
     * @var integer
     * @ORM\Column(name="Directory_id", type="integer", nullable=true)
     */
    private $directoryId;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return File
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
    
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return File
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set sizeAsString
     * 
     * @param string $sizeAsString
     *
     * @return File
     */
    public function setSizeAsString($sizeAsString)
    {
        $this->sizeAsString = $sizeAsString;

        return $this;
    }

    /**
     * Get sizeAsString
     *
     * @return string
     */
    public function getSizeAsString()
    {
        return $this->sizeAsString;
    }
    
    /**
     * Set size
     *
     * @param integer $size
     *
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    public function setUserId($user)
    {
        $this->userId = $user;

        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }
    
    public function getUploadedAt()
    {
        return $this->uploadedAt;
    }
    
    public function setUploadedAt()
    {
        $this->uploadedAt = new \DateTime("now");
        return $this;
    }

    public function setDirectoryId($directoryId)
    {
        $this->directoryId = $directoryId;

        return $this;
    }
    
    public function getDirectoryId()
    {
        return $this->directoryId;
    }

}

